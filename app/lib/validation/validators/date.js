const { validationError } = require("../../validation-error");
const { parse, isValid } = require('date-fns')

class DateValidator {
  constructor(options, errors) {
    this.options = {
      required: true,
    };

    Object.assign(this.options, options);

    this.fullYear = new Date().getFullYear();
    this.errors = {
      required: validationError("You must enter a value."),
      requiredDay: validationError("You must enter a value for day."),
      requiredMonth: validationError("You must enter a value for month."),
      requiredYear: validationError("You must enter a value for year."),
      invalidDate: validationError("You must enter a valid date."),
    };

    Object.assign(this.errors, errors);
  }

  validate(value) {
    const dateParts = value.split("/");

    // Count the non-empty parts
    const nonEmptyParts = dateParts.filter((part) => part.trim() !== "").length

    if (nonEmptyParts === 0){
      return this.errors.required;
    }

    // If day or month or year is missing
    if (dateParts[0].trim() === '') {
      return this.errors.requiredDay;
    } else if (dateParts[1].trim() === '') {
      return this.errors.requiredMonth;
    } else if (dateParts[2].trim() === '') {
      return this.errors.requiredYear;
    }

    // If all parts are provided, validate the date
    let parsedDate;
    try {
      parsedDate = parse(value, 'dd/MM/yyyy', new Date());
    } catch {
      return this.errors.invalidDate;
    }
    if (!isValid(parsedDate)) {
      return this.errors.invalidDate;
    }
  }
}

module.exports = DateValidator;
