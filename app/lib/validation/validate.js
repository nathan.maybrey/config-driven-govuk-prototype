/* eslint-disable no-param-reassign */
const logger = require("../logger");
const { fieldTypes } = require("../field-types");
const {
  TextValidator,
  DateValidator,
  CheckboxesValidator,
  RadioValidator,
  SelectValidator,
  MatchValidator,
} = require("./validators");

const handleValidation = (data, field, config) => {
  let validator = {};

  const validationOptions = config.options || {};

  switch (config.type) {
    case fieldTypes.textInput:
      validator = new TextValidator(validationOptions, config.errors);
      break;
    case fieldTypes.dateInput:
      validator = new DateValidator(validationOptions, config.errors);
      break;
    case fieldTypes.checkboxes:
      validator = new CheckboxesValidator(validationOptions, config.errors);
      break;
    case fieldTypes.radios:
      validator = new RadioValidator(validationOptions, config.errors);
      break;
    case fieldTypes.select:
      validator = new SelectValidator(validationOptions, config.errors);
      break;
    case fieldTypes.match:
      validator = new MatchValidator(validationOptions, config.errors);
      break;
    default:
      validator = new TextValidator(validationOptions, config.errors);
      break;
  }

  if (config.type === "dateInput") {
    const day = data[`${field}-day`];
    const month = data[`${field}-month`];
    const year = data[`${field}-year`];

    return validator.validate(`${day}/${month}/${year}`);
  } else if (config.type === "match") {
    return validator.validate(data[`${field}`], data[`${validationOptions.matchTo}`]);
  } else {
    return validator.validate(data[field]);
  }
};

module.exports.validate = (data, config, pageId) => {
  if (!config.validation) {
    logger.debug(
      `No validation defined in config for page: ${pageId}, continuing without validation`,
    );
    return null;
  }

  let errors = {};
  for (const [key, value] of Object.entries(config.validation)) {
    // Skip validation of current field if condition is not met
    if (value.condition) {
      logger.debug(`Condition found for field: ${key}, checking if condition is met`);
      const conditionAnswer = data[value.condition.field];
      logger.debug(`Condition answer: ${conditionAnswer}`);

      logger.debug(`Checking if ${conditionAnswer} does not include ${value.condition.value}`);
      if (!conditionAnswer?.includes(value.condition.value)) {
        logger.debug(`Condition not met, skipping validation for field: ${key}`);
        continue;
      }
    }
    const validationResult = handleValidation(data, key, value);
    if (validationResult) {
      errors[key] = validationResult;
    }
  }
  if (Object.keys(errors).length === 0) {
    errors = null;
  }
  return errors;
};
