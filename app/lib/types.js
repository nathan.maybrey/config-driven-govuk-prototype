const { matchTypes } = require("./match-types");
const { validationError } = require("./match-types");

/**
 * @typedef {Object} Condition
 * @property {string} field Field to check against condition.
 * @property {string | string[]} value Value to check against field.
 * @property {matchTypes} [match] Type of match to be made.
 */

/**
 * @typedef {Object} Next
 * @property {string} page Page to navigate to.
 * @property {Condition} [condition] Condition to be met to navigate to page.
 */

/**
 * @typedef {Object} Previous
 * @property {string} page Page to navigate to.
 * @property {Condition} [condition] Condition to be met to navigate to page.
 */

/**
 * @typedef {Object} Options
 */

/**
 * @typedef {Object} Validation
 * @property {string} type Field type to validate
 * @property {Condition} [condition] Condition to be met to validate the field.
 * @property {Options} [options] Options to customise for validation.
 * @property {Object} [errors] Overrides for error messages
 */

/**
 * @typedef {Object} Config Configuration for page in prototype.
 * @property {Previous[] | string} [previous] Previous page, or pages which can be navigated to. If there is more than one, conditions for each page will need to be unique.
 * @property {Next[] | string} [next] Next page, or pages which can be navigated to. If there is more than one, conditions for each page will need to be unique.
 * @property {Object.<string, Validation>} [validation] Validation to be run on fields on current page.
 */
