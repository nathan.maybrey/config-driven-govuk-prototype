/**
 * Constructs a validation error object.
 *
 * @param errorMsg {string|{ [summary]: string, [inline]: string }} - The error message to be returned to the user. Can be a string or an object.
 * If an object, must have either an inline or summary key, or both.
 *
 * @returns {{summary: string, inline: string}}
 * @throws {Error} - If the error message is not a string or an object, or if the error object does not have either
 */
module.exports.validationError = (errorMsg) => {
  if (typeof errorMsg === "string") {
    return { inline: errorMsg, summary: errorMsg };
  } else if (typeof errorMsg === "object") {
    if (errorMsg.inline && errorMsg.summary) {
      return errorMsg;
    } else if (errorMsg.inline || errorMsg.summary) {
      return {
        inline: errorMsg.inline || errorMsg.summary,
        summary: errorMsg.summary || errorMsg.inline,
      };
    } else {
      throw new Error("Error object must have either an inline or summary key, or both.");
    }
  } else {
    throw new Error("Error must be a string or an object.");
  }
};
