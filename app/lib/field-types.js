module.exports.fieldTypes = {
  checkboxes: "checkboxes",
  dateInput: "dateInput",
  radios: "radios",
  select: "select",
  textInput: "textInput",
  match: "match",
};
