/**
 * @enum {string}
 * A set of match types that can be used to match a value against a set of rules.
 * Can be used to help with routing, validation, and more.
 */
module.exports.matchTypes = {
  value: "value",
  anyOne: "any-one",
  all: "all",
  none: "none",
  includes: "includes",
  greaterThan: "greater-than",
  lessThan: "less-than",
};
