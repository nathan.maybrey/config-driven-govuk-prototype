//
// For guidance on how to create routes see:
// https://prototype-kit.service.gov.uk/docs/create-routes
//

const govukPrototypeKit = require("govuk-prototype-kit");
const router = govukPrototypeKit.requests.setupRouter();
const { handleRouting } = require("./lib/router");
const { validate } = require("./lib/validation/validate");

const getUrlMeta = (req) => {
  const fullPath = req.originalUrl;
  const pathSegments = fullPath.split("/");
  return {
    fullPath,
    lastSegment: pathSegments.pop(),
    pathBeforeLastSegment: pathSegments.join("/"),
  };
};

const getPageConfig = async (fullPath) => {
  try {
    return await require(`./page-configurations${fullPath}.js`);
  } catch (e) {
    console.error(
      `No page config found for page ${fullPath} or config is badly formatted, redirecting back to ${fullPath}`,
    );
    console.error(e);
  }
};

router.get("*", async function (req, res, next) {
  const { lastSegment } = getUrlMeta(req);
  res.locals.errors = req.session.data.validation?.[lastSegment];
  next();
});

// Handle post requests
router.post("*", async function (req, res) {
  const { data } = req.session;

  console.log("Session data: ", data);

  const { fullPath, pathBeforeLastSegment, lastSegment } = getUrlMeta(req);

  try {
    const pageConfig = await getPageConfig(fullPath);
    if (!pageConfig) {
      console.error(`No page config found, redirecting to ${fullPath}`);
      return res.redirect(fullPath);
    }

    const validationErrors = validate(data, pageConfig, lastSegment);

    if (validationErrors) {
      req.session.data.validation = {
        [lastSegment]: validationErrors,
      };
      console.error(`Validation errors for page, sending back to ${fullPath}`);
      return res.redirect(fullPath);
    }
    delete req.session?.data?.validation?.[lastSegment];

    const nextPage = handleRouting(pageConfig, data);
    return res.redirect(`${pathBeforeLastSegment}/${nextPage}`);
  } catch (e) {
    console.error("Error: ", e);
    return res.redirect(fullPath);
  }
});
