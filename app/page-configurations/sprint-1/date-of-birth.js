const { urls } = require("../../urls");
const { fieldTypes } = require("../../lib/field-types");
const { matchTypes } = require("../../lib/match-types");

/** @type {import('../../lib/types').Config} */
const config = {
  previous: urls.fullName,
  next: [
    {
      page: urls.dateOfBirthIneligible,
      condition: {
        field: "dateOfBirth-year",
        value: 1955,
        match: matchTypes.greaterThan,
      },
    },
    {
      page: urls.contactPreferences,
    },
  ],
  validation: {
    dateOfBirth: {
      type: fieldTypes.dateInput
    }
  }
};

module.exports = config;
