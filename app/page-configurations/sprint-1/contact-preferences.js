const { urls } = require("../../urls");
const { fieldTypes } = require("../../lib/field-types");

/** @type {import('../../lib/types').Config} */
const config = {
  previous: urls.start,
  next: urls.checkYourAnswers,
  validation: {
    contactPreferences: {
      type: fieldTypes.radios,
    },
    email: {
      type: fieldTypes.textInput,
      condition: {
        field: "contactPreferences",
        value: "email",
      },
    },
    phone: {
      type: fieldTypes.textInput,
      condition: {
        field: "contactPreferences",
        value: "phone",
      },
    },
    text: {
      type: fieldTypes.textInput,
      condition: {
        field: "contactPreferences",
        value: "text",
      },
    },
  },
};

module.exports = config;
